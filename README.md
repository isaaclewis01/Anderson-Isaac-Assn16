# Blackjack!

## NOTE
This was an assignment done for CS1400 at Utah State University, some code from the project was done by Chad Mano as a basic for the assignment, but near all of the code was completed by myself. I am only claiming ownership of what I have written, which is all, or near all, of the Python code.

## Manual
This program is run from the ass16-task1.py file in the src folder. This is a program that simulates the game Blackjack! After the program is run it will take you through all the necessary instructions to play the game, enjoy!
