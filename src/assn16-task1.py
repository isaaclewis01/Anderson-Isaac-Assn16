from deck import Deck
import time

def main():
    play = True

    # Variable defining/setup
    print(format("Welcome to Blackjack!", "^40"))
    players = eval(input("How many players (1-5, not including dealer)? "))
    print(" ")
    money = 100
    didBust = 0
    playerBet = 0
    bustCount = 0
    isBetValid = False
    continueRound = 0
    # Player number, money, points
    playerNum = [[0]]
    # What cards the player has
    playerHand = []
    handTotal = 0

    # Player setup, dealer player #0
    if 5 >= players >= 1:
        for i in range(1, players + 1):
            playerNum.append([i])

    # Game setup
    for i in range(1, (len(playerNum))):
        playerNum[i].append(money)
        playerNum[i].append(handTotal)
        playerNum[i].append(didBust)
        playerNum[i].append(playerBet)
        playerNum[0].append(handTotal)

    while play:
        newDeck = Deck()
        newDeck.shuffle()
        # Player bets
        for i in range(1, len(playerNum)):
            isBetValid = False
            print("Player " + str(playerNum[i][0]) + ":")
            print("\tAccount balance: " + str(playerNum[i][1]))
            while not isBetValid:
                bet = (eval(input("How much would you like to bet (minimum: $5 or your account balance if lower than $5)? ")))
                if (bet >= 5 or playerNum[i][1] <= 5) and playerNum[i][1] >= bet:
                    playerNum[i][4] = bet
                    print("Bet recorded.")
                    print(" ")
                    isBetValid = True
                else:
                    print("Bet not valid")
                    print(" ")
                    isBetValid = False

        # Card deal, dealer last place, player 1 in 0 position
        for i in range(0, len(playerNum)):
            playerHand.append([newDeck.draw()])
        for i in range(0, len(playerNum)):
            playerHand[i].append(newDeck.draw())
        # Display dealer's card
        print("The dealer's card is: ")
        print("\t" + str(playerHand[len(playerHand) - 1][0]))
        print(" ")

        # Ask players to hit or hold
        for i in range(0, (len(playerNum) - 1)):
            print("Player " + str(playerNum[i + 1][0]) + ", your hand is:")
            print("\t" + str(playerHand[i]))
            for j in range(len(playerHand[i])):
                # Test to see what cards are values at
                if 1 < playerHand[i][j].getCardValue() <= 10:
                    playerNum[i + 1][2] += playerHand[i][j].getCardValue()
                elif playerHand[i][j].getCardValue() > 10:
                    playerNum[i + 1][2] += 10
                else:
                    if playerNum[i + 1][2] + 11 > 21:
                        playerNum[i + 1][2] += 1
                    else:
                        playerNum[i + 1][2] += 11
            #print(playerNum[i + 1][2])
            hitHold = eval(input("Would you like to hit(0) or hold(1)? "))
            print(" ")
            # While the user wants to hit
            while hitHold == 0:
                playerHand[i].append(newDeck.draw())
                # Test to see what cards are values at
                if 1 < playerHand[i][len(playerHand[i])-1].getCardValue() <= 10:
                    playerNum[i + 1][2] += playerHand[i][len(playerHand[i])-1].getCardValue()
                elif playerHand[i][len(playerHand[i])-1].getCardValue() > 10:
                    playerNum[i + 1][2] += 10
                else:
                    if playerNum[i + 1][2] + 11 > 21:
                        playerNum[i + 1][2] += 1
                    else:
                        playerNum[i + 1][2] += 11
                if playerNum[i + 1][2] > 21:
                    print("Bust. Player " + str(playerNum[i + 1][0]) + " you lost this round.")
                    playerNum[i + 1][3] = 1
                    hitHold = 1
                    continueRound += 1
                else:
                    print("\tYour new hand is: " + str(playerHand[i]))
                    #print(playerNum[i + 1][2])
                    hitHold = eval(input("Would you like to hit(0) again or hold(1)? "))
                    print(" ")


        # Dealer turn
        print("The dealer's cards are: ")
        print("\t" + str(playerHand[len(playerHand) - 1][0]))
        print("\t" + str(playerHand[len(playerHand) - 1][1]))
        for j in range(len(playerHand[len(playerNum) - 1])):
            # Test to see what cards are values at
            if 1 < playerHand[len(playerHand) - 1][j].getCardValue() <= 10:
                playerNum[0][1] += playerHand[len(playerHand) - 1][j].getCardValue()
            elif playerHand[len(playerHand) - 1][j].getCardValue() > 10:
                playerNum[0][1] += 10
            else:
                playerNum[0][1] += 11

        while playerNum[0][1] < 17:
            playerHand[len(playerHand) - 1].append(newDeck.draw())
            time.sleep(1)
            print("The dealer takes a card...")
            print(" ")

            # Test to see what cards are valued at
            if 1 < playerHand[len(playerHand) - 1][len(playerHand[len(playerHand) - 1]) - 1].getCardValue() <= 10:
                playerNum[0][1] += playerHand[len(playerHand) - 1][len(playerHand[len(playerHand) - 1]) - 1].getCardValue()
            elif playerHand[len(playerHand) - 1][len(playerHand[len(playerHand) - 1]) - 1].getCardValue() > 10:
                playerNum[0][1] += 10
            else:
                if playerNum[0][1] + 11 > 21:
                    playerNum[0][1] += 1
                else:
                    playerNum[0][1] += 11

        # Test if dealer busts
        if playerNum[0][1] > 21:
            print("Dealer busts.")
            print(" ")
            print("All players who did not bust win! Which are:")
            for i in range(1, len(playerNum)):
                if playerNum[i][3] == 0:
                    print("\tPlayer " + str(playerNum[i][0]))
                    print(" ")
                else:
                    bustCount += 1
                if bustCount == 3:
                    print("\tNone.")
                    print(" ")
        else:
            print("Dealer holds.")
            print(" ")
            #print(playerNum[0][1])

        # Determining winners
        for i in range(1, len(playerNum)):
            if playerNum[0][1] == playerNum[i][2] and playerNum[i][3] != 1:
                print("Player " + str(playerNum[i][0]) + " tied with the dealer.")
                print("\tNeither adding nor subtracting the player's bet of " + str(playerNum[i][4]) + " to their account.")
                print(" ")
            elif (playerNum[0][1] > playerNum[i][2] or playerNum[i][2] > 21) and playerNum[0][1] <= 21:
                print("Player " + str(playerNum[i][0]) + " lost to the dealer.")
                print("\tSubtracting the player's bet of " + str(playerNum[i][4]) + " from their account.")
                print(" ")
                playerNum[i][1] -= playerNum[i][4]
            elif playerNum[0][1] < playerNum[i][2] and playerNum[i][2] < 22 and playerNum[i][3] != 1:
                print("Player " + str(playerNum[i][0]) + " beat the dealer.")
                print("\tAdding the player's bet of " + str(playerNum[i][4]) + " to their account.")
                print(" ")
                playerNum[i][1] += playerNum[i][4]
            elif playerNum[0][1] > 21 and playerNum[i][3] != 1:
                print("Dealer busts. Player " + str(playerNum[i][0]) + " beat the dealer.")
                print("\tAdding the player's bet of " + str(playerNum[i][4]) + " to their account.")
                print(" ")
                playerNum[i][1] += playerNum[i][4]
            elif playerNum[i][2] > 21:
                print("Player " + str(playerNum[i][0]) + " lost to the dealer for busting earlier.")
                print("\tSubtracting the player's bet of " + str(playerNum[i][4]) + " from their account.")
                print(" ")
                playerNum[i][1] -= playerNum[i][4]

        # Display user's balance
        for i in range(1, len(playerNum)):
            print("Player " + str(playerNum[i][0]) + ":")
            print("\tAccount balance: " + str(playerNum[i][1]))

        # Play again?
        playInput = input("Would you like to play again (Y/N)? ").lower()
        print(" ")
        if playInput == 'y':
            for i in range(1, len(playerNum) - 1):
                if playerNum[i][1] <= 0:
                    playerNum.pop(i)
            # Reset
            for i in range(1, len(playerNum)):
                playerNum[i][2] = 0
            for j in range(len(playerHand) - 1):
                playerHand.pop(j)
            playerNum[0][1] = 0
            playerHand.pop(len(playerHand) - 1)
        else:
            # Finish program
            print("Thank you for playing Blackjack!")
            print(" ")
            # Create list to keep track of money to sort out which player had the most money in the end
            moneyList = [[]]
            for i in range(1, len(playerNum)):
                moneyList.append([playerNum[i][1]])
                moneyList[i].append(playerNum[i][0])
            moneyList.sort(reverse=True)
            for i in range(len(playerNum) - 1):
                print("\tPlayer " + str(moneyList[i][1]) + " finished with $" + str(moneyList[i][0]))

            play = False
main()